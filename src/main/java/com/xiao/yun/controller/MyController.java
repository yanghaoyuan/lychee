package com.xiao.yun.controller;

import io.jboot.Jboot;
import io.jboot.component.metric.annotation.EnableMetricConcurrency;
import io.jboot.component.metric.annotation.EnableMetricCounter;
import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jboot.web.limitation.LimitRenderType;
import io.jboot.web.limitation.annotation.EnableRequestLimit;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author 袁旭云【rain.yuan@transn.com】
 * Created by rain on 2018/5/3.
 * @Date 2018/5/3 13:48
 */
@RequestMapping("/")
public class MyController extends JbootController {

    // 这里演示了 如何使用 spring 获取bean 也就是获取指定的暴露的dubbo服务
    private ApplicationContext context  = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");

    @EnableRequestLimit(rate = 1, renderType = LimitRenderType.TEXT, renderContent = "操作太快，请稍后再来。")
    public void index() {
        Jboot.me().getMetric().counter("index").inc();


        //renderText("hello jboot");
        setAttr("test","<script>alert('hello jboot')</script>");
        render("/pages/test/index.html");
    }

    public void test() {
        setAttr("test","hello jboot");
        render("/pages/test/index.html");
    }

    public static void main(String[] args) {
        Jboot.run(args);
    }
}