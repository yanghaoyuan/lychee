package com.xiao.yun.component.beelt;

import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.ext.web.WebRenderExt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * js,css 的版本编号
 *
 * @Author 袁旭云【rain.yuan@transn.com】
 * Created by rain on 2018/4/26.
 * @Date 2018/4/26 15:29
 */
public class GlobalExt implements WebRenderExt {
    static String version = "v331";

    @Override
    public void modify(Template template, GroupTemplate arg1, HttpServletRequest request, HttpServletResponse arg3) {
        //js,css 的版本编号
        template.binding("sysVersion", version);

        int port = request.getServerPort();
        String path = request.getContextPath();

        String basePath = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort() + path + "/";
        if (port == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        }

        template.binding("BASE_PATH", basePath);
        template.binding("ctx", basePath);

    }
}
